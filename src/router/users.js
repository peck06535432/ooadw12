const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')

// const User = require('/03_2/Soft Test/CRUDGroup/models/User')
//   // GET users listing.
// router.get('/', async (req, res, next) => {
//   // res.json(userController.getUsers())
// p1--------------------------------------------
//   User.find({}).exec(function (err, users) {
//     if (err) {
//       res.status(500).send()
//     }
//     res.json(users)
//   })
// p2----------------------------------------------
// User.find({}).then(function (err, users) {
//     res.json(users)
// }).catch(function (err) {
//  res.status(500).send(err)
// })
// p3------------------------------------------------
// Async Await
// try {
//   const users = await User.find({})
//   res.json(users)
// } catch (err) {
// res.status(500).send(err)
// }
// -------------------------------------------------
// })
// router.get('/:id', (req, res, next) => {
//   const { id } = req.params
// -------------------------------------------------
//   res.json(usersController.getUser(id))
// p1-----------------------------------------------
//   User.findById(id).then(function (user) {
//     res.json(user)
//   }).catch(function (err) {
//     res.ststus(500).send(err)
//   })
// p2-----------------------------------------------
//   try {
//     const user = await User.findBy(id)
//     res.json(user)
//   } catch(err) {
//     res.status(500).send(err)
//   }
// })
// // router.post('/', async (req, res, next) => {
//   const payload = req.body
//   const user = new User (payload)
//   try {
//     user.save()
//     res.json(newuser)
//   } catch (err) {
//     res.status(500).send(err)
//   }
// })
// router.put('/', async (req, res, next) => {
//   const payload = req.body
//  // res.json(usersController.updateUser(payload))
// try {
// const user = awiait User.updateOne({ _id: payload.id }, payload)
// res.json(user)
// } catch(err) {
// res.status(500).send(err)
// }
// })
// router.delete('/', async (req, res, next) => {
//   const { id } = req.params
//   // res.json(usersController.deleteUser(id))
//   try {
//     const user = await User.deleteOse({ _id: id })
//     res.json(user)
//   } catch (err) {
//     res.status(500).send(err)
//   }
// })

router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUser)

router.post('/', usersController.addUser)

router.put('/', usersController.updateUser)

router.delete('/:id', usersController.deleteUser)

module.exports = router
