import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/form',
    name: 'form',
    component: () => import('../views/Form.vue')
  },
  {
    path: '/showview',
    name: 'showview',
    component: () => import('../views/ShowView.vue')
  },
  {
    path: '/table',
    name: 'table',
    component: () => import('../views/Table.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/counter',
    name: 'counter',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Counter.vue')
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users')
  },
  {
    path: '/showupce',
    name: 'showupce',
    component: () => import('../views/ShowUPCE/ShowUserPassCE.vue')
  },
  {
    path: '/showa',
    name: 'showa',
    component: () => import('../views/ShowA/ShowsA.vue')
  },
  {
    path: '/showb',
    name: 'showb',
    component: () => import('../views/ShowA/ShowsB.vue')
  },
  {
    path: '/showc',
    name: 'showc',
    component: () => import('../views/ShowA/ShowsC.vue')
  },
  {
    path: '/ShowA/demo.vue',
    name: 'demo',
    component: () => import('../views/ShowA/demo.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
